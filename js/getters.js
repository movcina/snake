var defaults = 
	{
		inited: false,
		speed: 150,
		background: 'black',
		snakeBody: 'white',
		snakeHead: 'red',
		snakeHeadRadius: 5,
		snakeBodyRadius: 5,
		foodRadius: 10,
		V_POS: 10,
		V_NEG: -10,
		V_NULL: 0
		
	};


// checks if game has been initiated
function getInited()
{
	if(tizen.preference.exists('inited'))
	{
		return tizen.preference.getValue('inited');
	}
else 
	{
		return defaults.inited;	
	}
}

function setInited(value)
{
	tizen.preference.setValue('inited', value);
}
//****************************************





// speed of the worm
function getSpeed(){	
	if(tizen.preference.exists('speed'))
		{
			return tizen.preference.getValue('speed');
		}
	else 
		{
			return defaults.speed;	
		}
}

function setSpeed(value){
		tizen.preference.setValue('speed', value);	
}
//****************************************









// height of the window 360 for big, 320 for small
function getHeight()
{
	return window.innerWidth;
}

function getWidth()
{
	return window.innerWidth;
}
//****************************************









//background of the game
function setBackground(value)
{
	tizen.preference.setValue('background', value);
}

function getBackground()
{
	if(tizen.preference.exists('background'))
	{
		return tizen.preference.getValue('background');
	}
else 
	{
		return defaults.background;	
	}
}
//****************************************


//color of the snake head
function setSnakeHead(value)
{
	tizen.preference.setValue('snakehead', value);
}

function getSnakeHead()
{
	if(tizen.preference.exists('snakehead'))
	{
		return tizen.preference.getValue('snakehead');
	}
else 
	{
		return defaults.snakeHead;	
	}
}
//****************************************


//color of the snake head
function setSnakeBody(value)
{
	tizen.preference.setValue('snakebody', value);
}

function getSnakeHead()
{
	if(tizen.preference.exists('snakebody'))
	{
		return tizen.preference.getValue('snakebody');
	}
else 
	{
		return defaults.snakeBody;	
	}
}
//****************************************




//radius of snake
function setSnakeHeadRadius(value)
{
	tizen.preference.setValue('snakeradius', value);
}

function getSnakeHeadRadius()
{
	if(tizen.preference.exists('snakeradius'))
	{
		return tizen.preference.getValue('snakeradius');
	}
else 
	{
		return defaults.snakeHeadRadius;	
	}
}
//****************************************


//radius of snake
function setSnakeBodyRadius(value)
{
	tizen.preference.setValue('snakeradius', value);
}

function getSnakeBodyRadius()
{
	if(tizen.preference.exists('snakeradius'))
	{
		return tizen.preference.getValue('snakeradius');
	}
else 
	{
		return defaults.snakeBodyRadius;	
	}
}
//****************************************



//radius of food
function setFoodRadius(value)
{
	tizen.preference.setValue('foodradius', value);
}

function getFoodRadius()
{
	if(tizen.preference.exists('foodradius'))
	{
		return tizen.preference.getValue('foodradius');
	}
else 
	{
		return defaults.foodRadius;	
	}
}
//****************************************


//V_POS 
function setV_POS(value)
{
	tizen.preference.setValue('V_POS', value);
}

function getV_POS()
{
	if(tizen.preference.exists('V_POS'))
	{
		return tizen.preference.getValue('V_POS');
	}
else 
	{
		return defaults.V_POS;	
	}
}
//****************************************



//V_NEG 
function setV_POS(value)
{
	tizen.preference.setValue('V_NEG', value);
}

function getV_POS()
{
	if(tizen.preference.exists('V_NEG'))
	{
		return tizen.preference.getValue('V_NEG');
	}
else 
	{
		return defaults.V_NEG;	
	}
}
//****************************************


//V_NULL 
function setV_POS(value)
{
	tizen.preference.setValue('V_NULL', value);
}

function getV_POS()
{
	if(tizen.preference.exists('V_NULL'))
	{
		return tizen.preference.getValue('V_NULL');
	}
else 
	{
		return defaults.V_NULL;	
	}
}
//****************************************