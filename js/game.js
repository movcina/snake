var canvas = document.getElementById("snake");
var context = canvas.getContext('2d');
var WIDTH = 360;
var HEIGHT = 360;
canvas.height = HEIGHT;
canvas.width = WIDTH;
var middleY = canvas.height / 2;
var middleX = canvas.width / 2;
var radius = 5;
var BACKGROUND = 'black';
var SNAKE_MEMBER = 'white';
var SNAKE_HEAD = "red";
var speed = 150;

var V_POS = 10;
var V_NEG = -10;
var V_NULL = 0;

var dy = V_NULL;
var dx = V_POS;
var foodEaten = true;
var foodX;
var foodY;
var snakePositions = startSnake();
var exit = false;


// main game
clearScreen();
drawSnake();
drawFood();
main();
// end main game

// main loop
function main() {
	if(exit == true)
		return;
    setTimeout(
        function onTick() {
            clearScreen();
            drawFood();
            drawSnake();
            moveSnake();
            main();
            
        },
        speed
    );
}
// begin functions
function clearScreen() {
    context.fillStyle = BACKGROUND;
    context.beginPath();
    context.arc(middleX, middleY, canvas.height/2, 2*Math.PI,false);
    context.fill();
}
function drawSnake() {
    for (let i = 0; i < snakePositions.length; i++) {
        context.beginPath();
        context.arc(snakePositions[i].x, snakePositions[i].y, radius, 0, 2 * Math.PI, false);
        if (i === getHead()) {
            context.fillStyle = SNAKE_HEAD;
        } else {
            context.fillStyle = SNAKE_MEMBER;
        }
        context.fill();
    }
}

function drawFood() {
    if(foodEaten === true) {
        createFood();
        foodEaten = false;
    }
    context.beginPath();
    context.arc(foodX, foodY, radius, 0, 2 * Math.PI, false);
    context.fillStyle = SNAKE_MEMBER;
    context.fill();
}
function createFood() {
    foodX = Math.floor((Math.random()*canvas.width / 10)) * 10;
    foodY = Math.floor((Math.random()*canvas.height / 10)) * 10;
    // check if food is off screen
    if(getDistance(foodX,foodY) > middleX)
    {
        createFood();
    }
    // check if food is on snake
    for(let i = 0; i < snakePositions.length; i++)
    {
        if(snakePositions[i].x === foodX && snakePositions[i].y === foodY)
            createFood();
    }
    foodEaten = false;
}


function moveSnake() {
	// get next position
    let nextX = snakePositions[getHead()].x + dx;
    let nextY = snakePositions[getHead()].y + dy;
    
    if(getDistance(nextX,nextY) > middleX)
    {
        nextX = wrapAround(nextX,nextY).x;
        nextY = wrapAround(nextX,nextY).y;
    }



    // check if food ate
    if(nextX === foodX && nextY === foodY){
        foodEaten = true;
        //speed -= (speed*.05);
    }

    

    var nextHeadPos = {x: nextX, y: nextY};
    if(foodEaten === false) snakePositions.shift();
    snakePositions.push(nextHeadPos);
    // check if collision
    
    checkCollision();

}

function checkCollision()
{
    for(let i = 0; i < getHead() - 1; i++)
    {
        if(snakePositions[getHead()].x === snakePositions[i].x && snakePositions[getHead()].y === snakePositions[i].y)
        {
        	resetGame();
            
        }
    }
}
function resetGame(){
	alert("Game over.\nScore:" + snakePositions.length);
    dx = V_POS;
    dy = V_NULL;
    snakePositions = startSnake();
}

function getHead()
{
    return snakePositions.length - 1;
}

function getDistance(nextX,nextY) {
    return Math.sqrt(Math.pow((nextX-middleX) ,2) + Math.pow((nextY-middleY),2));
}
function wrapAround(nextX, nextY) {
    let newNextX, newNextY;
    if(dx !== V_NULL)
    {
        newNextY = nextY;
        newNextX = Math.abs(WIDTH - nextX);
    }else
    {
        newNextX = nextX;
        newNextY = Math.abs(HEIGHT - nextY);
    }
    return {x:newNextX,y:newNextY};
}
function startSnake(){
	return [{x: middleX - (radius * 6), y: middleY},
    {x: middleX - (radius * 4), y: middleY},
    {x: middleX - (radius * 2), y: middleY},
    {x: middleX, y: middleY}];
}

// begin event listener
document.addEventListener('rotarydetent',function(ev) {
    const KEY_PRESSED = ev.detail.direction;
    if (dx === 10)// going right
    {
        if (KEY_PRESSED == 'CW') {
            dx = V_NULL;
            dy = V_POS;
        } else if (KEY_PRESSED == 'CCW') {
            dx = V_NULL;
            dy = V_NEG;
        }
    } else if (dx === -10) {
        if (KEY_PRESSED == 'CW') {
            // go down
            dx = V_NULL;
            dy = V_NEG;
        } else if (KEY_PRESSED == 'CCW') {
            // go up
            dx = V_NULL;
            dy = V_POS;
        }
    } else if (dy === 10) {
        if (KEY_PRESSED == 'CW') {
            // go down
            dx = V_NEG;
            dy = V_NULL;
        } else if (KEY_PRESSED == 'CCW') {
            // go up
            dx = V_POS;
            dy = V_NULL;
        }
    } else if (dy === -10) {
        if (KEY_PRESSED == 'CW') {
            // go down
            dx = V_POS;
            dy = V_NULL;
        } else if (KEY_PRESSED == 'CCW') {
            // go up
            dx = V_NEG;
            dy = V_NULL;
        }
    }
});



