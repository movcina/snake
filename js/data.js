tizen.systeminfo.getPropertyValue("DISPLAY", onSuccessCallback, onErrorCallback);
var TIZEN_HEIGHT; 
var TIZEN_WIDTH;

// GET TIZEN DATA
function onSuccessCallback(display) {
           
    TIZEN_HEIGHT = display.resolutionHeight;
    TIZEN_WIDTH = display.resolutionWidth;
    console.log(TIZEN_HEIGHT + " <H in W> " + TIZEN_WIDTH);
}

function onErrorCallback(error) {
    alert("Not supported: " + error.message);
}

// END TIZEN DATA